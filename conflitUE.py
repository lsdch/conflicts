#!/usr/bin/env python3

from collections import Counter


def parseUE(fh):
    UE = dict()
    for line in fh:
        line = line.strip()
        if line == "***":
            break
        elif line:
            UE.update([line.split(maxsplit=1)])
    return UE


def init_conflict(UE: dict):
    conflict = dict()
    for ue in UE:
        conflict.update([[ue, Counter()]])
    return conflict


def count_conflict(fh, conflicts):
    ffwd = True
    for line in fh:
        # if line != "***":
        #     if ffwd is True:
        #         continue
        # else:
        #     ffwd = False

        line = line.strip().split()
        for elt in line:
            for conf in line:
                if conf != elt:
                    conflicts[elt][conf] += 1
    return conflicts


if __name__ == "__main__":
    with open("input.txt", "r") as fh:
        ue = parseUE(fh)
        conflicts = init_conflict(ue)
        conflicts = count_conflict(fh, conflicts)

        for cours, conf in conflicts.items():
            print("{}:".format(ue[cours]))
            for c, cnt in conf.items():
                print("\t{} : {}".format(cnt, ue[c]))
            print("")
